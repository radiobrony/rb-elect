import { createApp, h } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

import Admin from './components/Admin.js'

const app = createApp({
  render: () => h(Admin)
})
app.mount('#app')
