import { createApp, h } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

import App from './components/App.js'

const app = createApp({
  render: () => h(App)
})
app.mount('#app')
