export default {
  name: 'Admin',
  data () {
    return {
      j: null,
      socket: null
    }
  },
  mounted () {
    this.socket = new window.WebSocket('ws://localhost:9001')

    this.socket.addEventListener('open', _ => {
      this.socket.send('Coucou !')
    })

    this.socket.addEventListener('message', event => {
      if (event.origin !== 'ws://localhost:9001') return

      const dj = JSON.parse(event.data)
      if ('resultats' in dj) {
        this.j = dj
      }
    })
  },
  methods: {
    sendAll () {
      const jj = JSON.stringify(this.j)

      window.fetch('/data', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: jj
      })
        .then(r => r.text())
        .then(t => console.log(t))
        .catch(err => window.alert(err))
    }
  },
  computed: {
    totalPourcentage: function () {
      let sum = 0
      if (this.j !== null) {
        this.j.resultats.candidats.forEach(c => {
          sum += c.pourcentage
        })
      }
      return sum
    },
    totalPourcentage2: function () {
      let sum = 0
      if (this.j !== null) {
        this.j.resultats2.candidats.forEach(c => {
          sum += c.pourcentage
        })
      }
      return sum
    }
  },
  template: `
    <h1>Administration</h1>
    <div v-if="j !== null">
      <button @click="sendAll">Tout envoyer</button>

      <h2>Titres</h2>
      <label>Bannière</label> <input type="checkbox" v-model="j.infos.showInBanner"/><br/>
      <label>Titre</label> <input type="text" v-model="j.infos.titre" style="width: 305px;font-weight: bold;"/><br/>
      <label>Sous-titre</label> <input type="text" v-model="j.infos.sousTitre"/><br/>
      <label>Petite info</label> <input type="text" v-model="j.infos.infoTop"/>

      <h2>Participation/Abstention</h2>
      <label>Bannière</label> <input type="checkbox" v-model="j.abstention.showInBanner"/><br/>
      <label>Source</label> <input type="text" v-model="j.abstention.source"/><br/>
      <label>Date/Heure</label> <input type="text" v-model="j.abstention.heureEstimations"/><br/>
      <label>Pourcentage</label> <input type="number" step=".1" v-model="j.abstention.pourcentage"/><br/>
      <label>Participation</label> <input type="checkbox" v-model="j.abstention.isParticipation"/>

      <h2>Résultats 2e</h2>
      <label>Bannière</label> <input type="checkbox" v-model="j.resultats2.showInBanner"/><br/>
      <label>Plein écran</label> <input type="checkbox" v-model="j.resultats2.showFullscreen"/><br/>
      <label>Date/Heure</label> <input type="text" v-model="j.resultats2.heureEstimations"/><br/>
      <label>Source</label> <input type="text" v-model="j.resultats2.source"/><br/>
      <br/>
      <table>
        <thead>
          <tr>
            <th>Candidat</th>
            <th>Pourcentage</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(c, i) in j.resultats2.candidats" :key="c.id">
            <td>{{ c.prenom }} {{ c.nom }}</td>
            <td><input type="number" step=".1" v-model="c.pourcentage"/></td>
          </tr>
        </tbody>
        <tfoot>
          <tr style="font-weight: bold;">
            <td style="text-align: right;">Total</td>
            <td>{{ totalPourcentage2 }}%</td>
          </tr>
        </tfoot>
      </table>
      <br/>

      <h2>Résultats 1er</h2>
      <label>Bannière</label> <input type="checkbox" v-model="j.resultats.showInBanner"/><br/>
      <label>Plein écran</label> <input type="checkbox" v-model="j.resultats.showFullscreen"/><br/>
      <br/>
      <table>
        <thead>
          <tr>
            <th>Candidat</th>
            <th>Pourcentage</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(c, i) in j.resultats.candidats" :key="c.id">
            <td>{{ c.prenom }} {{ c.nom }}</td>
            <td><input type="number" step=".1" v-model="c.pourcentage"/></td>
          </tr>
        </tbody>
        <tfoot>
          <tr style="font-weight: bold;">
            <td style="text-align: right;">Total</td>
            <td>{{ totalPourcentage }}%</td>
          </tr>
        </tfoot>
      </table>
      <br/>
      <button @click="sendAll">Tout envoyer</button>
    </div>
  `
}
