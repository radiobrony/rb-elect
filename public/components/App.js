import ResultatSmall from './ResultatSmall.js'
import ResultatBig from './ResultatBig.js'

export default {
  name: 'App',
  components: {
    ResultatSmall,
    ResultatBig
  },
  data () {
    return {
      j: null,
      obsScene: '',
      socket: null,
      time: '00:00',
      bI: null,
      bC: 0,
      bCI: null,
      currentBanner: 'titre',
      banners: {
        titre: true,
        abst: false,
        resTitre: false,
        resTitre2: false,
        re: false,
        live: false,
        none: false
      },
      delayBanner: 10 // 24
    }
  },
  mounted () {
    this.clockTick()
    setInterval(this.clockTick, 1000)

    window.addEventListener('obsSceneChanged', event => {
      if (this.obsScene !== event.detail.name) {
        this.banners.live = (event.detail.name.includes('Screen') ||
                             event.detail.name.includes('Live'))
        this.obsScene = event.detail.name

        if (
          (
            this.banners.live && (
              this.currentBanner === 'resTitre' ||
              this.currentBanner === 'resTitre2'
            )
          ) ||
          (
            !this.banners.live && (
              this.j.resultats.showFullscreen ||
              this.j.resultats2.showFullscreen
            )
          )) {
          // Forcer le changement de la bannière
          this.checkBanner()

          // Reset le compteur
          clearInterval(this.bI)
          this.bI = setInterval(this.checkBanner, this.delayBanner * 1000)
        }
      }
    })

    this.socket = new window.WebSocket('ws://localhost:9001')

    this.socket.addEventListener('open', _ => {
      this.socket.send('Coucou !')
    })

    this.socket.addEventListener('message', event => {
      if (event.origin !== 'ws://localhost:9001') return

      const dj = JSON.parse(event.data)
      if ('resultats' in dj) {
        // Tri sur le pourcentage, décroissant
        dj.resultats.candidats.sort(
          (a, b) => (a.pourcentage < b.pourcentage) ? 1 : ((b.pourcentage < a.pourcentage) ? -1 : 0)
        )
        this.j = dj

        console.log(dj)

        // Changer la bannière de suite si nécessaire
        const c = this.currentBanner
        if (
          (c === 'titre' && !dj.infos.showInBanner) ||
          (c === 'abst' && !dj.abstention.showInBanner) ||
          (c === 'resTitre' && !dj.resultats.showFullscreen) ||
          (c === 'resTitre2' && !dj.resultats2.showFullscreen) ||
          (c === 'res' && dj.resultats2.showFullscreen) ||
          (c === 'res' && !dj.resultats2.showInBanner)
        ) {
          // Changer maintenant
          this.checkBanner()

          // Reset le compteur
          clearInterval(this.bI)
          this.bI = setInterval(this.checkBanner, this.delayBanner * 1000)
        }
      }
    })

    this.bI = setInterval(this.checkBanner, this.delayBanner * 1000)
  },
  methods: {
    pad (n) {
      if (n < 10) return '0' + n
      return n
    },
    clockTick () {
      const d = new Date()
      this.time = this.pad(d.getHours()) + ':' + this.pad(d.getMinutes())
    },
    rotateBanner (next) {
      // Désactiver la bannière actuelle
      this.banners[this.currentBanner] = false

      setTimeout(_ => {
        // Activer la bannière suivante
        this.banners[next] = true
        this.currentBanner = next

        /* Uniquement 1er tour
        if (next === 'res') {
          // Si on affiche les résultats en bad, faire tourner la liste
          this.bC = 0
          this.bCI = setInterval(_ => {
            if (this.j === null) return

            this.bC += 2
            if (this.bC >= this.j.resultats.candidats.length) this.bC = 0
          }, 4000)
        } else {
          // Désactiver le timer des résultats s'il est encore là
          clearInterval(this.bCI)
        }
        */
      }, 500)
    },
    checkBanner () {
      const c = this.currentBanner

      // C'est pas beau, mais ça fait ce que je veux
      if (c === 'titre') {
        if (this.j.abstention.showInBanner) {
          this.rotateBanner('abst')
        } else if (this.j.resultats2.showFullscreen && !this.banners.live) {
          this.rotateBanner('resTitre2')
        } else if (!this.j.resultats2.showFullscreen && this.j.resultats.showFullscreen && !this.banners.live) {
          this.rotateBanner('resTitre')
        } else if (this.j.resultats2.showInBanner && (!this.j.resultats2.showFullscreen || this.banners.live)) {
          this.rotateBanner('res')
        } else if (!this.j.infos.showInBanner) {
          this.rotateBanner('none')
        }
      } else if (c === 'abst') {
        if (this.j.resultats2.showFullscreen && !this.obsScene.includes('Live')) {
          this.rotateBanner('resTitre2')
        } else if (!this.j.resultats2.showFullscreen && this.j.resultats.showFullscreen && !this.banners.live) {
          this.rotateBanner('resTitre')
        } else if (this.j.resultats2.showInBanner && (!this.j.resultats2.showFullscreen || this.banners.live)) {
          this.rotateBanner('res')
        } else if (this.j.infos.showInBanner) {
          this.rotateBanner('titre')
        } else if (!this.j.abstention.showInBanner) {
          this.rotateBanner('none')
        }
      } else if (c === 'resTitre2') {
        if (this.j.infos.showInBanner) {
          this.rotateBanner('titre')
        } else if (this.j.abstention.showInBanner) {
          this.rotateBanner('abst')
        } else if (this.j.resultats2.showInBanner && (!this.j.resultats2.showFullscreen || this.banners.live)) {
          this.rotateBanner('res')
        } else if (!this.j.resultats2.showFullscreen) {
          this.rotateBanner('none')
        }
      } else if (c === 'resTitre') {
        if (this.j.infos.showInBanner) {
          this.rotateBanner('titre')
        } else if (this.j.abstention.showInBanner) {
          this.rotateBanner('abst')
        } else if (this.j.resultats2.showInBanner && (!this.j.resultats2.showFullscreen || this.banners.live)) {
          this.rotateBanner('res')
        } else if (!this.j.resultats.showFullscreen) {
          this.rotateBanner('none')
        }
      } else if (c === 'res') {
        if (this.j.infos.showInBanner) {
          this.rotateBanner('titre')
        } else if (this.j.abstention.showInBanner) {
          this.rotateBanner('abst')
        } else if (this.j.resultats2.showFullscreen && !this.banners.live) {
          this.rotateBanner('resTitre2')
        } else if (!this.j.resultats2.showFullscreen && this.j.resultats.showFullscreen && !this.banners.live) {
          this.rotateBanner('resTitre')
        } else if (!this.j.resultats2.showInBanner && !this.j.resultats2.showFullscreen) {
          this.rotateBanner('none')
        }
      } else {
        if (this.j.infos.showInBanner) {
          this.rotateBanner('titre')
        } else if (this.j.abstention.showInBanner) {
          this.rotateBanner('abst')
        } else if (this.j.resultats2.showFullscreen && !this.banners.live) {
          this.rotateBanner('resTitre2')
        } else if (!this.j.resultats2.showFullscreen && this.j.resultats.showFullscreen && !this.banners.live) {
          this.rotateBanner('resTitre')
        } else if (this.j.resultats2.showInBanner && (!this.j.resultats2.showFullscreen || this.banners.live)) {
          this.rotateBanner('res')
        }
      }
    }
  },
  template: `
    <div v-if="j !== null">
      <header>
        <transition-group
          :duration="{ enter: 500, leave: 200 }"
          name="revfade"
          mode="out-in">
          <div v-if="obsScene.includes('Live')" class="src-candidats">
            Images du candidat
          </div>
          <div v-else-if="j.infos.infoTop" class="top-info">
            {{ j.infos.infoTop }}
          </div>
        </transition-group>

        <!-- nice clock -->
        <div class="clock">{{ time }}</div>
      </header>

      <main>
        <aside>
          <!-- widgets/cams -->
        </aside>

        <article>
          <!-- écrans -->
          <transition-group
            :duration="{ enter: 500, leave: 200 }"
            name="fade"
            mode="out-in">
            <div class="resultats" v-if="j.resultats2.showFullscreen && !banners.live">
              <div class="gagnants">
                <resultat-big
                  v-for="c in j.resultats2.candidats"
                  :key="c.id"
                  :candidat="c"/>
              </div>
            </div>
            <div class="resultats" v-else-if="j.resultats.showFullscreen && !banners.live">
              <div class="gagnants">
                <resultat-big
                  v-for="c in j.resultats.candidats.slice(0, 2)"
                  :key="c.id"
                  :candidat="c"/>
              </div>

              <div class="reste">
                <resultat-small
                  v-for="c in j.resultats.candidats.slice(2)"
                  :key="c.id"
                  :candidat="c"/>
              </div>
            </div>
            <div class="logobg" v-else>
              <img src="assets/logo.png" alt="Radio Brony"/>
            </div>
          </transition-group>
        </article>
      </main>

      <footer>
        <!-- lower third -->
        <div class="logo">
          <img src="/assets/logo.png" alt="Radio Brony">
        </div>

        <transition-group
          :duration="{ enter: 500, leave: 200 }"
          name="fade"
          mode="out-in">
          <div class="banner title" v-if="banners.titre">
            <h1>{{ j.infos.titre }}</h1>
            <h2>{{ j.infos.sousTitre }}</h2>
          </div>
          
          <div class="banner abstention" v-if="banners.abst">
            <div class="affichage">
              <div class="texte">
                <template v-if="j.abstention.isParticipation">
                  Participation
                </template>
                <template v-else>
                  Abstention
                </template>
              </div>
              <div class="pourcentage">{{ j.abstention.pourcentage }}%</div>
            </div>

            <div class="estimation">
              <div class="heure">
                {{ j.abstention.heureEstimations }}
              </div>

              <div class="source">
                <template v-if="j.abstention.source !== ''">
                  Source : {{ j.abstention.source }}
                </template>
              </div>
            </div>
          </div>
          
          <div class="banner title center" v-if="banners.resTitre">
            <h1>Résultats du 1er tour</h1>
          </div>
          
          <div class="banner title center" v-if="banners.resTitre2">
            <h1>Estimation actuelle des résultats</h1>
    
            <div class="estimation">
              <div class="heure">
                {{ j.resultats2.heureEstimations }}
              </div>
    
              <div class="source">
                <template v-if="j.resultats2.source !== ''">
                  Source : {{ j.resultats2.source }}
                </template>
              </div>
            </div>
          </div>
          
          <div class="banner resultats" v-if="banners.res">
            <div class="candidats">
              <transition-group
                :duration="{ enter: 500, leave: 200 }"
                name="fade"
                mode="out-in">
                <resultat-small
                  v-for="c in j.resultats2.candidats.slice(bC, bC + 2)"
                  :key="c.id"
                  :candidat="c"/>
              </transition-group>
            </div>
    
            <div class="estimation">
              <div class="heure">
                {{ j.resultats2.heureEstimations }}
              </div>
    
              <div class="source">
                <template v-if="j.resultats2.source !== ''">
                  Source : {{ j.resultats2.source }}
                </template>
              </div>
            </div>
          </div>
        </transition-group>
      </footer>
    </div>
  `
}
