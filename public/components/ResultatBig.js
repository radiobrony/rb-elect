export default {
  name: 'ResultatBig',
  props: {
    candidat: Object
  },
  template: `
    <div class="candidat">
      <div class="top">
        <div
          class="pic"
          :style="{
            'border-color': candidat.couleur,
            'background-image': 'url(/assets/candidats/' + candidat.id + '.png)'
          }"></div>

        <div class="pourcentage">
          {{ candidat.pourcentage }}%
        </div>
      </div>

      <div class="details">
        <div class="nom">{{ candidat.prenom }} {{ candidat.nom }}</div>
        <div class="parti">{{ candidat.parti }}</div>
      </div>
    </div>
  `
}
