-- Ajouter la table des résultats du second tour
CREATE TABLE "resultats2" (
	"idCandidat"	INTEGER,
	"pourcentage"	NUMERIC DEFAULT 0.00,
	FOREIGN KEY("idCandidat") REFERENCES "candidats"("id"),
	UNIQUE("idCandidat")
);
-- Et ajouter des entrées vides avec Macron et Le Pen
INSERT INTO "main"."resultats2" ("idCandidat", "pourcentage") VALUES ('3', '0');
INSERT INTO "main"."resultats2" ("idCandidat", "pourcentage") VALUES ('5', '0');
