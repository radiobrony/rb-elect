const path = require('path')
const sqlite3 = require('sqlite3').verbose()
const express = require('express')
const bodyParser = require('body-parser')
const WebSocket = require('ws')
// deepcode ignore HttpToHttps: Only used locally so HTTPS is not necessary
const { createServer } = require('http')

// Init vars
let data = {
  infos: {
    showInBanner: true,
    titre: 'Emission Spéciale',
    sousTitre: 'radiobrony.fr',
    infoTop: ''
  },
  abstention: {
    showInBanner: false,
    heureEstimations: '18 heures',
    source: '-',
    pourcentage: 0,
    isParticipation: false
  },
  resultats: {
    showInBanner: false,
    showFullscreen: false,
    candidats: []
  },
  resultats2: {
    showInBanner: false,
    showFullscreen: false,
    heureEstimations: '20 heures',
    source: '-',
    candidats: []
  }
}

// --- Init objects ---
const jsonParser = bodyParser.json({ limit: '50mb' })
const db = new sqlite3.Database(path.join(__dirname, 'db.sqlite'), sqlite3.OPEN_READWRITE)

// --- Connect ---
// Start webserver
// deepcode ignore DisablePoweredBy, deepcode ignore UseCsurfForExpress: Only used locally so not necessary
const app = express()
// Return panel
app.use(express.static(path.join(__dirname, '/public')))
// Save our data
app.post('/data', jsonParser, (req, res) => {
  data = req.body
  pushUpdate(req.body)
  saveDB(req.body)
  res.send('OK')
})
// Return current data
app.get('/data', jsonParser, (_, res) => {
  // deepcode ignore XSS
  res.send(JSON.stringify(data))
})

// Start websocket
const server = createServer(app)
const wss = new WebSocket.Server({ server })

// Hey, listen!
server.listen(9001, _ => {
  console.log('Listening on :9001')
})

// --- Functions ---

function noop () {}

function heartbeat () {
  this.isAlive = true
}

function pushUpdate (d) {
  wss.clients.forEach(client => {
    try {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify(data))
      }
    } catch (err) {
      console.log('Error when pushing update to client:', err)
    }
  })
}

async function saveDB (d) {
  const perr = (_, err) => {
    if (err) console.log('SQL Error:', err)
  }

  // Mettre à jour les clés
  const stmtInfo = db.prepare('UPDATE infos SET value=? WHERE key=?')
  stmtInfo.run([d.infos.titre, 'titre'], perr)
  stmtInfo.run([d.infos.sousTitre, 'sous_titre'], perr)
  stmtInfo.run([d.abstention.heureEstimations, 'heure_abstention'], perr)
  stmtInfo.run([d.abstention.source, 'source_abstention'], perr)
  stmtInfo.run([d.abstention.pourcentage, 'abstention'], perr)
  stmtInfo.run([d.resultats2.heureEstimations, 'heure_resultats'], perr)
  stmtInfo.run([d.resultats2.source, 'source_resultats'], perr)
  stmtInfo.finalize(perr)

  // Puis le pourcentage des candidats
  const stmtRes = db.prepare('UPDATE resultats SET pourcentage=? WHERE idCandidat=?')
  // deepcode ignore HTTPSourceWithUncheckedType
  await d.resultats.candidats.forEach(c => {
    stmtRes.run([c.pourcentage, c.id], perr)
  })
  stmtRes.finalize(perr)

  // Puis le pourcentage des candidats
  const stmtRes2 = db.prepare('UPDATE resultats2 SET pourcentage=? WHERE idCandidat=?')
  // deepcode ignore HTTPSourceWithUncheckedType
  await d.resultats2.candidats.forEach(c => {
    stmtRes2.run([c.pourcentage, c.id], perr)
  })
  stmtRes2.finalize(perr)
}

function readDB () {
  db.all('SELECT key, value FROM infos', function (err, rows) {
    if (err) {
      console.log('Error when trying to get infos from DB:', err)
    } else {
      if (rows.length === 0) {
        console.error('Empty database')
      } else {
        rows.forEach(function (row) {
          switch (row.key) {
            case 'abstention':
              data.abstention.pourcentage = row.value
              break

            case 'heure_abstention':
              data.abstention.heureEstimations = row.value
              break

            case 'source_abstention':
              data.abstention.source = row.value
              break

            case 'heure_resultats':
              data.resultats2.heureEstimations = row.value
              break

            case 'source_resultats':
              data.resultats2.source = row.value
              break

            case 'titre':
              data.infos.titre = row.value
              break

            case 'sous_titre':
              data.infos.sousTitre = row.value
              break

            default:
              console.log('Unknown key:', row.key)
              break
          }
        })
      }
    }
  })

  db.all(
    'SELECT id, prenom, nom, parti, couleur, pourcentage FROM candidats JOIN resultats ON resultats.idCandidat=candidats.id',
    function (err, rows) {
      data.resultats.candidats = []

      if (err) {
        console.log('Error when trying to get resultats from DB:', err)
      } else {
        if (rows.length === 0) {
          console.error('Empty database')
        } else {
          rows.forEach(function (row) {
            // deepcode ignore HTTPSourceWithUncheckedType
            data.resultats.candidats.push({
              id: row.id,
              prenom: row.prenom,
              nom: row.nom,
              parti: row.parti,
              couleur: row.couleur,
              pourcentage: row.pourcentage
            })
          })
        }
      }
    }
  )

  db.all(
    'SELECT id, prenom, nom, parti, couleur, pourcentage FROM candidats JOIN resultats2 ON resultats2.idCandidat=candidats.id',
    function (err, rows) {
      data.resultats2.candidats = []

      if (err) {
        console.log('Error when trying to get resultats from DB:', err)
      } else {
        if (rows.length === 0) {
          console.error('Empty database')
        } else {
          rows.forEach(function (row) {
            // deepcode ignore HTTPSourceWithUncheckedType
            data.resultats2.candidats.push({
              id: row.id,
              prenom: row.prenom,
              nom: row.nom,
              parti: row.parti,
              couleur: row.couleur,
              pourcentage: row.pourcentage
            })
          })
        }
      }
    }
  )
}

// On WS connection
wss.on('connection', ws => {
  ws.send(JSON.stringify(data))

  ws.on('message', (data, isBinary) => {
    const message = isBinary ? data : data.toString()

    if (message === 'srvtime') {
      const tsnow = { ts: Date.now() }
      ws.send(JSON.stringify(tsnow))
    } else {
      ws.send(JSON.stringify(data))
    }
  })

  ws.isAlive = true
  ws.on('pong', heartbeat)
})

// Close dead WS connections
setInterval(_ => {
  wss.clients.forEach(ws => {
    try {
      if (ws.isAlive === false) {
        ws.terminate()
      } else {
        ws.isAlive = false
        ws.ping(noop)
      }
    } catch (err) {
      console.log('Error on checking deads:', err)
    }
  })
}, 30000)

readDB()
