# RB Habillage Elections
> Edition 2022 (2e tour)

# Installation

- Installez [NodeJS] (au moins la version 14)
- Récupérer le code source (avec un `git clone` ou en téléchargeant le [zip])
- Copiez `db.sqlite.dist` vers `db.sqlite`
- Ouvrez un terminal dans le dossier du code ([Astuce Windows])
    - Installez les dépendances: `npm i` (préférez `yarn` s'il est installé)
    - Lancez le serveur: `node server.js`


# Utilisation

- L'administration est accessible sur <http://localhost:9001/admin.html>
- Dans OBS, ajoutez une source navigateur
    - URL: `http://localhost:9001/`
    - Hauteur: 1080
    - Largeur: 1920
- Plus de notes [dans le wiki]


[NodeJS]: https://nodejs.org/fr/
[zip]: https://gitlab.com/radiobrony/rb-elect/-/archive/main/rb-elect-main.zip
[Astuce Windows]: https://www.pcastuces.com/pratique/astuces/5379.htm
[dans le wiki]: https://gitlab.com/radiobrony/rb-elect/-/wikis/home
